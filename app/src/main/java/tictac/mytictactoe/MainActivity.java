package tictac.mytictactoe;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import tictac.logic.CpuPlayer;
import tictac.logic.TicTacToe;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    TableLayout board;
    Button exitButton;
    ArrayList<ImageButton> slots = new ArrayList<>();
    TicTacToe game = new TicTacToe();
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        create_board();

        exitButton = (Button) findViewById(R.id.button_exit);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopService(new Intent(this, MusicService.class));
        // TODO: make onPause
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this, MusicService.class));
        // TODO: make onResume
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_new_game:
                if(Build.VERSION.SDK_INT > 10) {
                    this.recreate(); // reset the activity
                } else {
                    Intent i = new Intent(this, MainActivity.class);
                    startActivity(i);
                    finish();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void create_board() {
        int count = 0;
        int screenHeight = getWindowManager().getDefaultDisplay().getHeight();

        board = (TableLayout) findViewById(R.id.board);
        for (int i = 0; i < 3; i++) {
            TableRow row = new TableRow(this);
            LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT, (float) 1.0);
            LayoutParams ButtonParams = new LayoutParams(0, (int)(screenHeight*0.25), (float) 1.0);

            row.setLayoutParams(params);

            for (int j = 0; j < 3; j++) {
                ImageButton button = new ImageButton(this);
                button.setLayoutParams(ButtonParams);
                button.setTag(count);
                //button.setHeight();
                button.setMaxHeight((int) Math.floor(screenHeight * 0.25));
                //button.setTextColor(Color.BLACK);
                button.setScaleType(ImageView.ScaleType.FIT_CENTER);
                this.slots.add(button);

                row.addView(button);
                count++;
            }

            board.addView(row);
        }

        // add listeners to buttons
        for (ImageButton b : slots) {
            b.setOnClickListener(this);
        }


        Log.d("createLayout", "end of create board function");
        Log.d("createLayout", "screen:" + (String.valueOf(screenHeight)) + "px");
    }

    // click listener for the slot buttons
    @Override
    public void onClick(View v) {
        CpuPlayer cpu;
        Resources res = getResources();
        ImageButton button = (ImageButton) v;

        // Player's turn
        if(game.play(TicTacToe.PLAYER, (Integer) button.getTag())) {
            // add player image to the button
            Drawable drawablePlayer = res.getDrawable(R.drawable.player);
            button.setImageBitmap(((BitmapDrawable) drawablePlayer).getBitmap());


            try {
                // now is the turn of the cpu
                cpu = new CpuPlayer(game.getFreeSlots());
                int move = cpu.getMove();
                if(move >= 0 && move < 9){
                    game.play(TicTacToe.CPU, move);
                    // add cpu image on button
                    Drawable drawableCpu = res.getDrawable(R.drawable.cpu);
                    button = slots.get(move); // retrieve from arrayList
                    button.setImageBitmap(((BitmapDrawable) drawableCpu).getBitmap());
                }


                // check if someone has won
                switch (game.checkGame()){
                    case TicTacToe.PLAYER:
                        setAlertDialog("You win!");
                        alertDialog.show();
                        break;
                    case TicTacToe.CPU:
                        setAlertDialog("Cpu wins!");
                        alertDialog.show();
                        break;
                    case 't':
                        setAlertDialog("It is a tie!");
                        alertDialog.show();
                    case 'e':
                        // Dummy case: nobody has won yet
                    default:
                }


                // logs
                Log.d("logic", "cpu movement: " + move);
                Log.d("logic", "board: " + new String(game.getBoard()));
            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
                Log.d("logic", "exception: " + e.getLocalizedMessage());
                Log.d("logic", "exception: " + e.getCause().getMessage());
            }
        }


    }

    private void setAlertDialog(String message){
        alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Game Over");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "New game", new
                DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.this.recreate();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Exit", new
                DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MainActivity.this.finish();
            }
        });
    }

}
