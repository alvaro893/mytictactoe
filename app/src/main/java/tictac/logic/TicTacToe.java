package tictac.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Alvaro on 04/05/2015.
 */
public class TicTacToe {

    protected char board[];
    protected final int BOARD_SIZE = 9;

    public final static char PLAYER = 'x';
    public final static char CPU = 'o';
    public final static char EMPTY = 'e';

    public TicTacToe(){
        board = new char[BOARD_SIZE];
        clearBoard();

    }

    public boolean play(char who, int where){

        if(board[where] != EMPTY)
            return false;
        else
            board[where] = who;

        return true;

    }

    public char[] getBoard(){
        return board;
    }
    public void clearBoard(){
        for(int i = 0; i < BOARD_SIZE; i++){
            board[i] = 'e'; // e from empty
        }
    }

    public boolean isFull(){
        int count;
        for(count = 0; count < BOARD_SIZE; count++){
            if(board[count] != PLAYER || board[count] != CPU)
                break;
        }

        return count >= BOARD_SIZE;
    }

    public Integer[] getFreeSlots(){
        List<Integer> stillFree = new ArrayList<>();

        for(int i = 0; i < BOARD_SIZE; i++){
            if(board[i] == 'e'){
                stillFree.add(i);
            }
        }
        if(stillFree.isEmpty()){
            return null;
        }else {
            // conversion to array of Integers
            Object[] arr = stillFree.toArray();
            Integer[] Intarr = Arrays.copyOf(arr, arr.length, Integer[].class);
            return Intarr;
        }
    }

    public char checkGame(){
        char[] gamer = {PLAYER, CPU};
        
        for(char c : gamer){
             // colunms
            for (int i = 0; i < 3; i++){
                if(board[i] == c && board[i+3] == c && board[i+6] == c)
                    return c;
            }
            // rows
            for (int i = 0; i < 6; i += 3){
                if(board[i] == c && board[i+1] == c && board[i+2] == c)
                    return c;
            }

            // diagonals
            if(board[2] == c && board[4] == c && board[6] == c)
                return c;
            if(board[0] == c && board[4] == c && board[8] == c)
                return c;
        }

        // in case of tie
        if(isFull())
            return 't';

        // nobody has won yet
        return 'e';
    }


}
