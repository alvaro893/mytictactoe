package tictac.logic;

import java.util.Random;

/**
 * Created by Alvaro on 04/05/2015.
 */
public class CpuPlayer {

    private Integer[] freeMoves;

    public CpuPlayer(Integer[] freeMoves) /*throws Exception*/ {
        if(freeMoves != null)
            this.freeMoves = freeMoves;
//        else {
//            throw new Exception("there are no more free moves");
//        }
    }

    public int getMove() {
        if (freeMoves != null) {
            Random r = new Random();
            int newPosition = r.nextInt(freeMoves.length);

            return freeMoves[newPosition];
        }

        return -1;
    }
}
